<?php

namespace Drupal\Tests\file_to_media\Functional;

use Drupal\Tests\file\Functional\FileFieldTestBase;
use Drupal\Tests\media\Traits\MediaTypeCreationTrait;
use Drupal\file\FileInterface;
use Drupal\file\Entity\File;
use Drupal\media\MediaTypeInterface;
use Drupal\node\Entity\NodeType;
use Drupal\views\Entity\View;

/**
 * Private test file for File to Media module.
 *
 * @group file
 */
class PrivateFileTest extends FileFieldTestBase {

  use MediaTypeCreationTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'node_access_test',
    'field_test',
    'file_to_media',
    'media',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A private file entity for testing.
   *
   * @var \Drupal\file\FileInterface
   */
  protected FileInterface $privateFile;

  /**
   * A media type for testing.
   *
   * @var \Drupal\media\MediaTypeInterface
   */
  protected MediaTypeInterface $mediaType;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    node_access_test_add_field(NodeType::load('article'));
    node_access_rebuild();
    \Drupal::state()->set('node_access_test.private', TRUE);
    // This test expects unused managed files to be marked as a temporary file.
    $this->config('file.settings')
      ->set('make_unused_managed_files_temporary', TRUE)
      ->save();

    // Create an admin user for this test.
    $this->adminUser = $this->drupalCreateUser([
      'access files overview',
      'bypass node access',
      'administer media',
      'access media overview',
    ]);

    // Add the file to media links to the file overview.
    $this->setUpFileToMediaLinks();
    // Create a private file.
    $this->createPrivateFile();
    // Create the document media type.
    $this->mediaType = $this->createMediaType('file');
  }

  /**
   * Tests creating a media entity for a private file.
   */
  public function testPrivateFile() {
    $assert = $this->assertSession();
    // Logout the admin user.
    $this->drupalLogOut();
    // Ensure the file cannot be downloaded after logging out.
    $this->drupalGet($this->privateFile->createFileUrl(FALSE));
    $assert->statusCodeEquals(403);

    // Login as an admin user.
    $this->drupalLogin($this->adminUser);
    // Check view.
    $this->drupalGet('admin/content/files');
    // The path to create a new media for the private file.
    $path_create_file_media = '/file/to-media/' . $this->privateFile->id() . '/' . $this->mediaType->id();
    // The file to media link should not be available for a private file.
    $assert->linkByHrefNotExists($path_create_file_media);
    $link_title = sprintf('Create %s', $this->mediaType->label());
    $assert->linkNotExists($link_title);
    // Test the route to create a new media for the private file directly.
    $this->drupalGet($path_create_file_media);
    $assert->statusCodeEquals(403);
  }

  /**
   * Set up the file to media links in the file overview.
   */
  private function setUpFileToMediaLinks() {
    $view = View::load('files');
    $displays = $view->get('display');
    // Add in the file_to_media field.
    $displays['default']['display_options']['fields']['file_to_media'] = [
      'id' => 'file_to_media',
      'table' => 'file_managed',
      'field' => 'file_to_media',
      'relationship' => 'none',
      'group_type' => 'group',
      'admin_label' => '',
      'label' => 'Create media',
      'exclude' => FALSE,
      'alter' => [
        'alter_text' => FALSE,
        'text' => '',
        'make_link' => FALSE,
        'path' => '',
        'absolute' => FALSE,
        'external' => FALSE,
        'replace_spaces' => FALSE,
        'path_case' => 'none',
        'trim_whitespace' => FALSE,
        'alt' => '',
        'rel' => '',
        'link_class' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'nl2br' => FALSE,
        'max_length' => '0',
        'word_boundary' => TRUE,
        'ellipsis' => TRUE,
        'more_link' => FALSE,
        'more_link_text' => '',
        'more_link_path' => '',
        'strip_tags' => FALSE,
        'trim' => FALSE,
        'preserve_tags' => '',
        'html' => FALSE,
      ],
      'element_type' => '',
      'element_class' => '',
      'element_label_type' => '',
      'element_label_class' => '',
      'element_label_colon' => TRUE,
      'element_wrapper_type' => '',
      'element_wrapper_class' => '',
      'element_default_classes' => TRUE,
      'empty' => '',
      'hide_empty' => FALSE,
      'empty_zero' => FALSE,
      'hide_alter_empty' => TRUE,
      'entity_type' => 'file',
      'plugin_id' => 'file_to_media',
    ];
    $view->set('display', $displays);
    $view->save();
  }

  /**
   * Create a private file for testing.
   */
  private function createPrivateFile() {
    $node_storage = $this->container->get('entity_type.manager')->getStorage('node');
    $type_name = 'article';
    $field_name = strtolower($this->randomMachineName());
    $this->createFileField($field_name, 'node', $type_name, ['uri_scheme' => 'private']);
    $test_file = $this->getTestFile('text');
    $nid = $this->uploadNodeFile($test_file, $field_name, $type_name, TRUE, ['private' => TRUE]);
    \Drupal::entityTypeManager()->getStorage('node')->resetCache([$nid]);
    /** @var \Drupal\node\NodeInterface $node */
    $node = $node_storage->load($nid);
    $this->privateFile = File::load($node->{$field_name}->target_id);
  }

}
